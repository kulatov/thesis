organization := "lrd"

name := "thesis"

version := "1.0-SNAPSHOT"

// =====================================================================================================================

scalaVersion := "2.11.7"

scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-explaintypes", "-encoding", "UTF8",
  "-Xlint", "-Xfatal-warnings")

resolvers ++= Seq(
  "Eclipse Paho Repo" at "https://repo.eclipse.org/content/repositories/paho-releases/"
)

libraryDependencies ++= Seq(
  // == JAVA ==
  "ch.qos.logback"          %  "logback-classic"                % "1.1.3",
  "org.postgresql"          %  "postgresql"                     % "9.4-1204-jdbc42",
  // == SCALA ==
  "com.typesafe.akka"       %% "akka-actor"                     % "2.4.0",
  "com.typesafe.akka"       %% "akka-slf4j"                     % "2.4.0"
)

// =====================================================================================================================

