package object thesis {

  def con[T, S](obj: T)(op: T => S) = {
    op(obj)
    obj
  }

  def using[T <: { def close(): Unit }, S](obj: T)(op: T => S) = {
    import scala.language.reflectiveCalls

    try {
      op(obj)
    } finally {
      obj.close()
    }
  }

}
