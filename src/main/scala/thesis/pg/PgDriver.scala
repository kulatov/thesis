package thesis.pg

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet, SQLException}

import akka.actor.Actor.Receive
import org.postgresql.jdbc42.Jdbc42ResultSet
import thesis._

import scala.util.Try

trait PgDriver extends PgTypes {

  // ORDER BY clause
  //  ex.: Ordering(Seq(1, 2, -3), 10, 100) == ORDER BY 1, 2, 3 DESC LIMIT 10 OFFSET 100
  protected case class Ordering(orderBy: Seq[Int] = Seq.empty, limit: Int = 0, offset: Int = 0)

  // all about connection (one conn for driver, no conn pool here because of seq of operations)
  private var db: Connection = null

  protected final def conn(p: PgConnParams): String = {
    Class.forName("org.postgresql.Driver").newInstance()
    var url = s"jdbc:postgresql://${p.server}/${p.database}?ApplicationName=${p.appName}"
    if (p.ssl)
      url += "&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory"
    db = DriverManager.getConnection(url, p.user, p.passwd)
    // call version() for additionally validate the connection
    call("version", r = PG_TEXT)
  }

  // for use in Actor-based ORMs
  protected final def withValidatedConn(p: PgConnParams)(f: Receive): Receive = {
    case x if f.isDefinedAt(x) =>
      try {
        if (db == null ) throw new SQLException() // just reconn
        using (db.prepareStatement("select 1"))(_.executeQuery().close())
      }
      catch {
        case e: SQLException => Try(conn(p)) // silently reconn
      }
      f(x)
  }

  // universal parameters processor for any statement type (offs is for func only)
  private final def Parametrize(ps: PreparedStatement, p: Seq[Product] = Seq.empty, offs: Int = 1) {
    offs until p.size + offs foreach { i =>
      p(i - offs) match {
        // value with simple type...
        // ... null/not null ...
        case (t: Int, v: Option[Any]) =>
          v.fold(ps.setNull(i, t))(some => ps.setObject(i, some, t))
        // ... not null ...
        case (t: Int, v: Any) =>
          ps.setObject(i, v, t)
        // ... null
        case (t: Int, null) =>
          ps.setNull(i, t)
        // array of explicitly named postgres type (t)
        // TODO case (t: String, v: Option[Array[AnyRef]]) => v.fold(stmt.setNull(i + offs, Types.ARRAY))(some => stmt.setArray(i + offs, db.createArrayOf(t, some)))
        case (t: String, v: Array[AnyRef]) =>
          ps.setArray(i, db.createArrayOf(t, v))
        // null value for array
        case (t: String, null) =>
          ps.setNull(i, PG_ARRAY)
        // errors
        case (t: Any, v: Any) =>
          throw new Exception(s"[PgDriver.Parametrize] type -> value error ('${t.getClass}' -> '${v.getClass}')")
        case a: Any =>
          throw new Exception(s"[PgDriver.Parametrize] ERRER: '$a' ")
      }
    }
  }

  // function CALL
  protected final def call[A](f: String, p: Seq[Product] = Seq.empty, r: Int = PG_NULL): A = {
    val proc = r == PG_NULL
    using(db.prepareCall(s"{ ${if (proc) "" else "?="} call $f(${Seq.fill(p.size)('?').mkString(",")}) }")) { c =>
      // set result, process parameters, execute
      if (!proc) c.registerOutParameter(1, r)
      Parametrize(c, p, if (proc) 1 else 2)
      c.execute()
      // process result
      if (proc) ().asInstanceOf[A]
      else if (r == PG_ARRAY) c.asInstanceOf[Jdbc42ResultSet].getArray(1).asInstanceOf[A]
      else c.getObject(1).asInstanceOf[A]
    }
  }

  // INSERT / UPDATE / DELETE
  protected final def dml(stmt: String, params: Seq[Product] = Seq.empty) = {
    using(db.prepareStatement(stmt)) { ps =>
      Parametrize(ps, params)
      ps.executeUpdate()
    }
  }

  // SELECT / INSERT+returning private base (warning! Iterator is not iterating here, so wrappers required)
  private final def queryImpl[A, B](q: String, p: Seq[Product] = Seq.empty, o: Ordering = Ordering())
                                   (row: (ResultSet) => (A))
                                   (fetch: (Iterator[A] => (B))) = {
    // format ordering
    val ord = if (o.orderBy == Seq.empty) ""
    else // orderBy should be declared to use limit/offset
      " order by " + o.orderBy.map(col => if (col > 0) col.toString else (-col).toString + " desc").mkString(",") +
        (if (o.limit == 0) "" else s" limit ${o.limit}") +
        (if (o.offset == 0) "" else s" offset ${o.offset}")

    // prepare/execute
    using(db.prepareStatement(q + ord)) { s =>
      Parametrize(s, p)
      using(s.executeQuery()) { rs =>
        // postprocess data, generating result
        fetch(new Iterator[A] {
          def hasNext = rs.next()
          def next() = row(rs)
        })
      }
    }
  }

  // INSERT+returning helpers
  protected final def insertRetOne[A](ins: String, params: Seq[Product] = Seq.empty)(r: (ResultSet) => (A)): A = {
    selectOne(ins, params)(r) // strictly fetch only one value
  }

  protected final def insertRetList[A](ins: String, params: Seq[Product] = Seq.empty)(r: (ResultSet) => (A)): List[A] = {
    select(ins, params)(r)
  }

  // SELECT helpers
  protected final def select[A](query: String, params: Seq[Product] = Seq.empty, ordering: Ordering = Ordering())
                               (r: (ResultSet) => (A)): List[A] = {
    queryImpl(query, params, ordering)(r)(_.toList)
  }

  protected final def selectMap[A, B](query: String, params: Seq[Product] = Seq.empty, ordering: Ordering = Ordering())
                                     (r: (ResultSet) => (A, B)): Map[A, B] = {
    queryImpl(query, params, ordering)(r)(_.toMap)
  }

  protected final def selectOne[A](query: String, params: Seq[Product] = Seq.empty)
                                  (r: (ResultSet) => (A)): A = {
    queryImpl(query, params)(r)(f => { if (!f.hasNext) throw PgNoDataFoundException else f.next() }) // strictly fetch only one value
  }

  protected final def selectOneOption[A](query: String, params: Seq[Product] = Seq.empty)
                                        (r: (ResultSet) => (A)): Option[A] = {
    queryImpl(query, params)(r)(f => { if (f.hasNext) Some(f.next()) else None }) // strictly fetch only one value
  }

  // several useful routines

  protected final def getMetaData = db.getMetaData

  protected final def dbNow = call[java.util.Date]("now", r = PG_TIMESTAMP)

  // string context ====================================================================================================
  /*implicit class __psql__(val sc: StringContext) extends AnyVal{
    def psql(args: Any*): String = {
      throw new NotImplementedError("__psql__")
    }
  }*/

}