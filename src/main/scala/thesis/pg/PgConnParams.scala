package thesis.pg

final case class PgConnParams(appName: String, server: String, database: String,
                              user: String, passwd: String, ssl: Boolean = false)
