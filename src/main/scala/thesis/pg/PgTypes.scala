package thesis.pg

private[pg] trait PgTypes {
  import java.sql.Types

  // aliases for types
  protected val PG_NULL       = Types.NULL

  protected val PG_INT        = Types.INTEGER
  protected val PG_BIGINT     = Types.BIGINT
  protected val PG_NUMERIC    = Types.DECIMAL

  protected val PG_TEXT       = Types.VARCHAR
  protected val PG_UUID       = Types.OTHER

  protected val PG_DATE       = Types.DATE
  protected val PG_TIMESTAMP  = Types.TIMESTAMP

  protected val PG_BYTEA      = Types.LONGVARBINARY

  protected val PG_ARRAY      = Types.ARRAY
}
